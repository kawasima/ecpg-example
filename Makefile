TARGET = main
SRC = $(TARGET).pgc
C_SRC = $(SRC:%.pgc=%.c)
OBJ = $(C_SRC:%.c=%.o)

all: $(OBJ)
	gcc -o $(TARGET) $(OBJ) -lecpg

clean:
	rm -f $(OBJ) $(TARGET) $(C_SRC)

.SUFFIXES: .o .c .pgc

.pgc.c:
	ecpg $< -I/usr/include/pgsql

.c.o:
	gcc -c $< -I/usr/include/pgsql
