# README #

## Postgresqlを起動する

```
% docker pull postgres:13.2-alpine
%  docker run -it --rm --name ecpg-example -e POSTGRES_PASSWORD=postgres -p 5432:5432 postgres:13.2-alpine
```

## ビルド&実行

```
% make
```

実行

```
% ./main
```

中間ファイルの削除

```
% make clean
```
